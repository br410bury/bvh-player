# Description #

This application was written with the intent of being a general purpose BVH player for Java (see http://www.dcs.shef.ac.uk/intranet/research/public/resmes/CS0111.pdf for an explanation of the BVH format). It require LWJGL (http://lwjgl.org/) to run and can be easily embedded in an applet or application. The application is supplied under the MIT license (summarized [here](https://tldrlegal.com/license/mit-license) - see text below). A Main.java and NetBeans project is supplied to illustrate usage.

# Authorship #

Written by BR410bury ([br410bury@gmail.com](mailto:br410bury@gmail.com)) in 7/2012 and uploaded to BitBucket in 7/2014.

# MIT License #

Copyright (C) 2012 BR410bury

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.