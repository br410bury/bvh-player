package main;

import java.io.IOException;
import net.br410bury.formats.BvhFormat;
import net.br410bury.display.desktop.BvhPanel;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import org.lwjgl.LWJGLException;

public class Main
{
	public static void main(String args[]) throws IOException, LWJGLException
	{
		BvhFormat bvh = new BvhFormat();
		JFrame bvhframe = new JFrame("Test");
		BvhPanel bvhpanel = new BvhPanel(bvhframe, bvh);
		
		bvh.read("var/ex.bvh");
		bvh.save("var/test.bvh");
		bvh.read("var/swordplay.bvh");
		
		bvhframe.setLayout(new BorderLayout());
		bvhframe.add(bvhpanel);
		bvhframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		bvhframe.setSize(640, 480);
		bvhpanel.start();
		bvhframe.setVisible(true);
	}
}