package net.br410bury.graphics;

import net.br410bury.spacetime.Handedness;
import net.br410bury.spacetime.Transform3D;

public class GraphicsMatrix
{
	protected double[][] matrix = new double[4][4];
	protected String name = null;
	protected Handedness hand = Handedness.RIGHT;

	
	
	/**
	 * Creates a new empty right-handed matrix.
	 */
	public GraphicsMatrix()
	{
		matrix = new double[4][4];
		for (int i = 0; i < 4; i++) matrix[i][i] = 1;
	}

	/**
	 * Copies the data in copy into the internal representation of this matrix.
	 * 
	 * @param copy The data to copy.
	 */
	public GraphicsMatrix(double[][] copy)
	{
		matrix = new double[4][4];
		for (int i = 0; i < 4; i++) System.arraycopy(copy[i], 0, matrix[i], 0, 4);
	}

	/**
	 * The copy constructor.
	 * 
	 * @param copy The matrix to copy.
	 */
	public GraphicsMatrix(GraphicsMatrix copy)
	{
		matrix = copy.getCopy();
		hand = copy.getHandedness();
	}

	
	
	/**
	 * Clones this object.
	 * 
	 * @return A clone of this object.
	 */
	public GraphicsMatrix clone()
	{
		return new GraphicsMatrix(this);
	}
	
	/**
	 * Returns the internal representation of this matrix.
	 * 
	 * @return The internal representation of this matrix.
	 */
	public double[][] get()
	{
		return matrix;
	}
	
	/**
	 * Returns a deep copy of the internal representation of this matrix.
	 * 
	 * @return A deep copy of the internal representation of this matrix.
	 */
	public double[][] getCopy()
	{
		double[][] copy = new double[4][4];

		for (int i = 0; i < 4; i++) System.arraycopy(matrix[i], 0, copy[i], 0, 4); 

		return copy;
	}

	/**
	 * Multiplies the supplied point by the current transformation matrix and returns the
	 * result.
	 * 
	 * @param point The point to transform.
	 * @return The transformed point.
	 */
	public GraphicsPoint times(GraphicsPoint point)
	{
		return new GraphicsPoint(times(point.get()));
	}

	/**
	 * Multiplies the supplied point by the current transformation matrix and returns the
	 * result.
	 * 
	 * @param point The point to transform.
	 * @return The transformed point.
	 */
	public double[] times(double[] point)
	{
		double[] result = new double[4];
		double[] mult = new double[4];
		
		mult = GraphicsPoint.homogeneousPoint(point);
		for (int row = 0; row < 4; row++)
		{
			for (int k = 0; k < 4; k++) result[row] += matrix[row][k] * mult[k];
		}
		
		return result;
	}

	/**
	 * Converts matrix to an identity matrix.
	 * 
	 * @return This object after being converted into an identity matrix.
	 */
	public GraphicsMatrix eye()
	{
		matrix = new double[4][4];
		for (int i = 0; i < 4; i++) matrix[i][i] = 1;
		
		return this;
	}

	/**
	 * Returns the currently set handedness of this matrix.
	 * 
	 * @return The currently set handedness of this matrix.
	 */
	public Handedness getHandedness()
	{
		return this.hand;
	}
	
	/**
	 * Performs the given rotation on this matrix such that, if this matrix is H, the
	 * result is RH.
	 * 
	 * @param rad The amount to rotate.
	 * @param type The direction to rotate.
	 */
	public void rotate(double rad, Transform3D type)
	{
		if(type.isRotation()) transform(rad, type);
	}

	/**
	 * Performs one rotation for each type given. The final result is such that,
	 * given the transformation matrix T_i, corresponding the a matrix of type types[i]
	 * and of the amount amount[i], the final result is (T_n)...(T_3)(T_2)(T_1). In other
	 * words, the transformations are applied in reverse order.
	 * 
	 * @param rad The amounts to transform corresponding to the types.
	 * @param types The types of transformation.
	 */
	public void rotate(double[] rad, Transform3D[] types)
	{
		for (int i = 0; i < types.length; i++)
		{
			if(types[i].isRotation()) transform(rad[i], types[i]);
		}
	}

	/**
	 * Sets the handedness of this matrix to either right or left handed coordinate
	 * systems.
	 * 
	 * @param hand The handedness of this graphic matrices' coordinate system.
	 */
	public void setHandedness(Handedness hand)
	{
		this.hand = hand;
	}
	
	/**
	 * Multiplies this graphics matrix by the supplied double matrix.
	 * 
	 * @param mult The matrix to multiply by.
	 * @return If this matrix is A, and @mult is B, returns the matrix AB.
	 */
	public GraphicsMatrix times(double[][] mult)
	{
		double[][] result = new double[4][4];

		for (int row = 0; row < 4; row++)
		{
			for (int col = 0; col < 4; col++)
			{
				for (int k = 0; k < 4; k++)
				{
					result[row][col] += matrix[row][k] * mult[k][col];
				}
			}
		}

		return new GraphicsMatrix(result);
	}

	/**
	 * Performs multiplication between this matrix and the supplied graphics matrix.
	 * 
	 * @param mult The matrix to multiply by.
	 * @return If this matrix is A, and @mult is B, returns the matrix AB.
	 */
	public GraphicsMatrix times(GraphicsMatrix mult)
	{
		return times(mult.get());
	}

	/**
	 * Returns a string representation of this object.
	 * 
	 * @return A string representation of this object.
	 */
	public String toString()
	{
		String str = "";

		if (name != null)
		{
			str += name;
		}
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				str += String.format("%9.4f ", matrix[i][j]);
			}
			str += "\n";
		}

		return str;
	}

	/**
	 * Performs one transformation for each type given. The final result is such that,
	 * given the transformation matrix T_i, corresponding the a matrix of type types[i]
	 * and of the amount amount[i], the final result is (T_n)...(T_3)(T_2)(T_1). In other
	 * words, the transformations are applied in reverse order.
	 * 
	 * @param amount The amounts to transform corresponding to the types.
	 * @param types The types of transformation.
	 */
	public void transform(double[] amount, Transform3D[] types)
	{
		for (int i = types.length-1; i >= 0; i--)
		{
			transform(amount[i], types[i]);
		}
	}

	/**
	 * Performs the given transformation by the given amount.
	 * 
	 * @param amount The amount to transform this matrix.
	 * @param type The type of transformation to perform.
	 */
	public void transform(double amount, Transform3D type)
	{
		switch (type)
		{
			case XRot:  xRotate(amount);    break;
			case YRot:  yRotate(amount);    break;
			case ZRot:  zRotate(amount);    break;
			case XTran: xTranslate(amount); break;
			case YTran: yTranslate(amount); break;
			case ZTran: zTranslate(amount); break;
		}
	}

	/**
	 * Translates along the Transform3D type axis by the given amount.
	 * 
	 * @param amount The amount to translate
	 * @param type The type of translation.
	 */
	public void translate(double amount, Transform3D type)
	{
		if(type.isTranslation()) transform(amount, type);
	}

	/**
	 * Performs one translation for each type given. The final result is such that,
	 * given the transformation matrix T_i, corresponding the a matrix of type types[i]
	 * and of the amount amount[i], the final result is (T_n)...(T_3)(T_2)(T_1). In other
	 * words, the transformations are applied in reverse order.
	 * 
	 * @param amount The amounts to transform corresponding to the types.
	 * @param types The types of transformation.
	 */
	public void translate(double[] amount, Transform3D[] types)
	{
		for (int i = types.length-1; i >= 0; i--)
		{
			if(types[i].isTranslation()) transform(amount[i], types[i]);
		}
	}
	
	/**
	 * Translates by the given quantities along their respective axis.
	 * 
	 * @param x The amount to translate along the x-axis.
	 * @param y The amount to translate along the y-axis.
	 * @param z The amount to translate along the z-axis.
	 */
	public void translate(double x, double y, double z)
	{
		xTranslate(x);
		yTranslate(y);
		zTranslate(z);
	}

	/**
	 * Rotates by the given amount and by the set handed coordinate system rules arond
	 * the x-axis.
	 * 
	 * @param rad The amount to rotate.
	 */
	public void xRotate(double rad)
	{
		double[][] result = new double[4][4];
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		double mult = hand.multiplier();

		System.arraycopy(matrix[0], 0, result[0], 0, 4);
		System.arraycopy(matrix[3], 0, result[3], 0, 4);
		for (int i = 0; i < 4; i++)
		{
			result[1][i] = cos * matrix[1][i] - mult * sin * matrix[2][i];
			result[2][i] = cos * matrix[2][i] + mult * sin * matrix[1][i];
		}

		matrix = result;
	}

	/**
	 * Translate the matrix by x units in the x direction.
	 * 
	 * @param x The number of units to translate along x.
	 */
	public void xTranslate(double x)
	{
		matrix[0][3] += x;
	}

	/**
	 * Rotates by the given amount and by the set handed coordinate system rules arond
	 * the y-axis.
	 * 
	 * @param rad The amount to rotate.
	 */
	public void yRotate(double rad)
	{
		double[][] result = new double[4][4];
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		double mult = hand.multiplier();

		System.arraycopy(matrix[1], 0, result[1], 0, 4);
		System.arraycopy(matrix[3], 0, result[3], 0, 4);
		for (int i = 0; i < 4; i++)
		{
			result[0][i] = cos * matrix[0][i] + mult * sin * matrix[2][i];
			result[2][i] = cos * matrix[2][i] - mult * sin * matrix[0][i];
		}

		matrix = result;
	}

	/**
	 * Translate the matrix by y units in the y direction.
	 * 
	 * @param y The number of units to translate along y.
	 */
	public void yTranslate(double y)
	{
		matrix[1][3] += y;
	}

	/**
	 * Rotates by the given amount and by the set handed coordinate system rules arond
	 * the z-axis.
	 * 
	 * @param rad The amount to rotate.
	 */
	public void zRotate(double rad)
	{
		double[][] result = new double[4][4];
		double cos = Math.cos(rad);
		double sin = Math.sin(rad);
		double mult = hand.multiplier();

		System.arraycopy(matrix[2], 0, result[2], 0, 4);
		System.arraycopy(matrix[3], 0, result[3], 0, 4);
		for (int i = 0; i < 4; i++)
		{
			result[0][i] = cos * matrix[0][i] - mult * sin * matrix[1][i];
			result[1][i] = cos * matrix[1][i] + mult * sin * matrix[0][i];
		}

		matrix = result;
	}

	/**
	 * Translate the matrix by z units in the z direction.
	 * 
	 * @param z The number of units to translate along z.
	 */
	public void zTranslate(double z)
	{
		matrix[2][3] += z;
	}
}