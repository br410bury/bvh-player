package net.br410bury.graphics;

public class GraphicsPoint
{
	protected double[] point;
	
	
	
	/**
	 * Creates a new graphics point at the origin.
	 */
	public GraphicsPoint()
	{
		point = new double[4];
		point[3] = 1;
	}
	
	/**
	 * Creates a new graphics point from the 3 or 4 element array copy.
	 * 
	 * @param copy If copy has 3 elements, sets the x, y, and z of this point to copy[0],
	 *             copy[1], and copy[2] respectively and then sets w = 1.  If copy has
	 *             4 elements, sets x, y, z, and w to copy[0], copy[1], copy[2], and 
	 *             copy[3] respectively.
	 */
	public GraphicsPoint(double[] copy)
	{
		point = new double[4];
		point[3] = 1;
		System.arraycopy(copy, 0, point, 0, copy.length);
	}
	
	/**
	 * Creates a new graphics point with the values {x, y, z, 1}.
	 * 
	 * @param x The x location of this point.
	 * @param y The y location of this point.
	 * @param z The z location of this point.
	 */
	public GraphicsPoint(double x, double y, double z)
	{
		point = new double[4];
		
		point[0] = x;
		point[1] = y;
		point[2] = z;
		point[3] = 1;
	}
	
	/**
	 * Copies the data in the passed in graphics point into this graphics point.
	 * @param point 
	 */
	public GraphicsPoint(GraphicsPoint point)
	{
		this(point.get());
	}
	
	
	
	/**
	 * Given two points, returns the minimum x, y, and z values from those points.
	 * 
	 * @param a The first point.
	 * @param b The second point.
	 * @return The minimum x, y, and z from the points a and b.
	 */
	public static GraphicsPoint min(GraphicsPoint a, GraphicsPoint b)
	{
		GraphicsPoint result = new GraphicsPoint();
		
		result.setX(a.getX() < b.getX() ? a.getX() : b.getX());
		result.setY(a.getY() < b.getY() ? a.getY() : b.getY());
		result.setZ(a.getZ() < b.getZ() ? a.getZ() : b.getZ());
		
		return result;
	}
	
	/**
	 * Given two points, returns the maximum x, y, and z values from those points.
	 * 
	 * @param a The first point.
	 * @param b The second point.
	 * @return The maximum x, y, and z from the points a and b.
	 */
	public static GraphicsPoint max(GraphicsPoint a, GraphicsPoint b)
	{
		GraphicsPoint result = new GraphicsPoint();
		
		result.setX(a.getX() > b.getX() ? a.getX() : b.getX());
		result.setY(a.getY() > b.getY() ? a.getY() : b.getY());
		result.setZ(a.getZ() > b.getZ() ? a.getZ() : b.getZ());
		
		return result;
	}
	
	/**
	 * Returns the (x,y,z) location that is at the center of the line formed by [a,b].
	 * 
	 * @param a The first point.
	 * @param b The second point.
	 * @return The (x,y,z) location that is at the center of the line formed by [a,b].
	 */
	public static GraphicsPoint center(GraphicsPoint a, GraphicsPoint b)
	{
		GraphicsPoint center = new GraphicsPoint();
		
		center.setX((a.getX() + b.getX()) / 2.0);
		center.setY((a.getY() + b.getY()) / 2.0);
		center.setZ((a.getZ() + b.getZ()) / 2.0);
		
		return center;
	}
	
	
	
	/**
	 * Creates a deep copy of this object.
	 * 
	 * @return A deep copy of this object.
	 */
	public GraphicsPoint clone()
	{
		return new GraphicsPoint(this);
	}
	
	/**
	 * Returns the local data of this object.
	 * 
	 * @return The local data of this object.
	 */
	public double[] get()
	{
		return this.point;
	}
	
	/**
	 * Returns a copy of the local data of this object.
	 * 
	 * @return A copy of the local data of this object.
	 */
	public double[] getCopy()
	{
		double[] copy = new double[4];
		
		System.arraycopy(copy, 0, point, 0, 4);
		
		return copy;
	}
	
	/**
	 * Returns the x component of this graphics point.
	 * 
	 * @return The x component of this graphics point.
	 */
	public double getX()
	{
		return point[0];
	}
	
	/**
	 * Returns the y component of this graphics point.
	 * 
	 * @return The y component of this graphics point.
	 */
	public double getY()
	{
		return point[1];
	}
	
	/**
	 * Returns the z component of this graphics point.
	 * 
	 * @return The z component of this graphics point.
	 */
	public double getZ()
	{
		return point[2];
	}
	
	/**
	 * Adds two graphic points together.
	 * 
	 * @param b The point to add to this point.
	 * @return If this point is a, returns a new point that is the result of a + b.
	 */
	public GraphicsPoint plus(GraphicsPoint b)
	{
		double x, y, z;
		
		x = point[0] + b.get()[0];
		y = point[1] + b.get()[1];
		z = point[2] + b.get()[2];
		
		return new GraphicsPoint(x, y, z);
	}
	
	/**
	 * Sets point to the value supplied by the passed in point.
	 * 
	 * @param point An array of doubles of the form {x,y,z} or {x,y,z,w}.
	 */
	public void set(double[] point)
	{
		System.arraycopy(this.point, 0, point, 0, point.length);
	}
	
	/**
	 * Sets the x-coordinate of this point.
	 * 
	 * @param x The x-coordinate of this point.
	 */
	public void setX(double x)
	{
		point[0] = x;
	}
	
	/**
	 * Sets the y-coordinate of this point.
	 * 
	 * @param y The y-coordinate of this point.
	 */
	public void setY(double y)
	{
		point[1] = y;
	}
	
	/**
	 * Sets the z-coordinate of this point.
	 * 
	 * @param z The z-coordinate of this point.
	 */
	public void setZ(double z)
	{
		point[2] = z;
	}
	
	/**
	 * Returns a string representation of this object.
	 * 
	 * @return A string representation of this object.
	 */
	public String toString()
	{
		String str = "";
		
		for(int i = 0; i < 3; i++) str += String.format("%10.4f ", point[i]);
		
		return str;
	}
	
	public static double[] homogeneousPoint(double[] point)
	{
		double[] result = new double[4];
		
		result[3] = 1;
		System.arraycopy(point, 0, result, 0, point.length);
		
		return result;
	}
}