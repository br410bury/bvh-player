package net.br410bury.formats;

import net.br410bury.motion.Motion;
import net.br410bury.motion.Joint;
import net.br410bury.motion.Bone;

public abstract class MotionFormat extends FormatFile
{
	protected Bone[] rootbone = null;
	protected Joint root = null;
	protected Motion motion = null;
	protected int count = 0;
	
	
	
	public abstract void segment();
	public abstract void segment(int frame);
	
	
	
	/**
	 * Returns the motion for this file.
	 * 
	 * @return The motion.
	 */
	public Motion getMotion()
	{
		return motion;
	}
	
	/**
	 * Returns the root joint from which the entire skeleton is built.
	 * 
	 * @return The root joint.
	 */
	public Joint getRoot()
	{
		return root;
	}
	
	/**
	 * Returns the root bone for the given frame.
	 * 
	 * @param frame The frame to retrieve the root bone for.
	 * @return The root bone for the given frame.
	 */
	public Bone getSkeleton(int frame)
	{
		return (rootbone == null ? null : rootbone[frame]);
	}
	
	/**
	 * Tells whether or not this motion file is ready for processing or not.
	 * 
	 * @return Whether a file has been successfully read or not.
	 */
	public boolean isRead()
	{
		return read;
	}
	
	/**
	 * Sets the motion to the motion being passed in.
	 * 
	 * @param mot The motion to use for this motion file.
	 */
	public void setMotion(Motion motion)
	{
		this.motion = new Motion(motion.getData());
	}
	
	/**
	 * Sets the root joint to the requested joint.
	 * 
	 * @param root The root joint for a skeleton.
	 * @param count The number of joins in the skeleton.
	 */
	public void setRoot(Joint root, int count)
	{
		this.root = root;
		this.count = count;
	}
}