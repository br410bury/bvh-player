package net.br410bury.formats;

import java.io.IOException;

public abstract class FormatFile
{
	protected boolean read = false;
	
	public abstract void read(String filename) throws IOException;
	public abstract void save(String filename) throws IOException;
}