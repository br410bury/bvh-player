package net.br410bury.motion;

import net.br410bury.graphics.GraphicsPoint;
import java.util.ArrayList;

public class Bone
{
	protected GraphicsPoint start;
	protected ArrayList<Bone> ends;
	
	
	
	/**
	 * Creates an empty bone with the origin for the starting point.
	 */
	public Bone()
	{
		this.start = new GraphicsPoint();
		this.ends = new ArrayList<Bone>();
	}
	
	/**
	 * Creates a bone with the given starting point.
	 * 
	 * @param start The starting point for this bone.
	 */
	public Bone(GraphicsPoint start)
	{
		this.start = start;
		this.ends = new ArrayList<Bone>();
	}
	
	
	
	/**
	 * Adds the given bone to the end points of this bone.
	 * 
	 * @param bone A single bone end point.
	 */
	public void addEnd(Bone bone)
	{
		ends.add(bone);
	}
	
	/**
	 * Adds an end point for this bone. A given bone can have multiple vertices.
	 * 
	 * @param end A single end point for this bone.
	 */
	public void addEnd(GraphicsPoint end)
	{
		ends.add(new Bone(end));
	}
	
	/**
	 * Returns the end points of this bone.
	 * 
	 * @return The end points of this bone.
	 */
	public ArrayList<Bone> getEnds()
	{
		return ends;
	}
	
	/**
	 * Finds the maximum x, y, and z location in this skeleton.
	 * 
	 * @return The maximum x, y, and z location in this skeleton.
	 */
	public GraphicsPoint getMaxes()
	{
		GraphicsPoint maxes = new GraphicsPoint(start.get());
		
		for(Bone child : ends) maxes = child.getMaxes(maxes);
		
		return maxes;
	}
	
	/**
	 * Given a point that is the current max, finds if any point in this skeleton has a higher
	 * x, y, or z coordinate than the currently given max.
	 * 
	 * @param maxes The currently given max.
	 * @return The maximum of either this skeletons x, y, and z components or the point maxes.
	 */
	protected GraphicsPoint getMaxes(GraphicsPoint maxes)
	{
		if(start.getX() > maxes.getX()) maxes.setX(start.getX());
		if(start.getY() > maxes.getY()) maxes.setY(start.getY());
		if(start.getZ() > maxes.getZ()) maxes.setZ(start.getZ());
		
		for(Bone child : ends) maxes = child.getMaxes(maxes);
		
		return maxes;
	}
	
	/**
	 * Finds the minimum x, y, and z location in this skeleton.
	 * 
	 * @return The minimum x, y, and z location in this skeleton.
	 */
	public GraphicsPoint getMins()
	{
		GraphicsPoint mins = new GraphicsPoint(start.get());
		
		for(Bone child : ends) mins = child.getMins(mins);
		
		return mins;
	}
	
	/**
	 * Given a point that is the current min, finds if any point in this skeleton has a lower
	 * x, y, or z coordinate than the currently given min.
	 * 
	 * @param maxes The currently given max.
	 * @return The minimum of either this skeletons x, y, and z components or the point mins.
	 */
	protected GraphicsPoint getMins(GraphicsPoint mins)
	{
		if(start.getX() < mins.getX()) mins.setX(start.getX());
		if(start.getY() < mins.getY()) mins.setY(start.getY());
		if(start.getZ() < mins.getZ()) mins.setZ(start.getZ());
		
		for(Bone child : ends) mins = child.getMins(mins);
		
		return mins;
	}
	
	/**
	 * Sets the beginning of the bone.
	 * 
	 * @return The beginning of the bone.
	 */
	public GraphicsPoint getStart()
	{
		return start;
	}
	
	/**
	 * Sets the beginning of the bone.
	 * 
	 * @param start The starting point for this bone.
	 */
	public void setStart(GraphicsPoint start)
	{
		this.start = start;
	}
	
	/**
	 * Returns a string representation of this object.
	 * 
	 * @return A string representation of this object.
	 */
	public String toString()
	{
		String str = "";
		
		for(Bone end : ends)
		{
			str += "(" + start + ") - (" + end.getStart() + ")\n";
			str += end;
		}
		
		return str;
	}
}