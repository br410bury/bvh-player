package net.br410bury.motion;

public class Motion
{
	protected double[][] motions;
	protected double frametime;
	
	
	
	/**
	 * Creates a copy of the passed in data.
	 * 
	 * @param copy The data to copy.
	 */
	public Motion(double[][] copy)
	{
		motions = new double[copy.length][copy[0].length];
		for(int frame = 0; frame < copy.length; frame++)
		{
			System.arraycopy(copy[frame], 0, motions[frame], 0, copy[frame].length);
		}
	}
	
	/**
	 * Creates a motion with the given number of frames and channels.
	 * 
	 * @param frames The number of frames this motion will have.
	 * @param channels The number of channels each frame will have.
	 */
	public Motion(int frames, int channels)
	{
		motions = new double[frames][channels];
	}
	
	/**
	 * Creates a motion with the given number of frames and channels.
	 * 
	 * @param frames The number of frames this motion will have.
	 * @param channels The number of channels each frame will have.
	 * @param length How long each frame should last.
	 */
	public Motion(int frames, int channels, int length)
	{
		motions = new double[frames][channels];
		frametime = length;
	}
	
	
	
	/**
	 * Returns the data stored in this motion.
	 * 
	 * @return The data stored in this motion.
	 */
	public double[][] getData()
	{
		return this.motions;
	}
	
	/**
	 * Returns a copy of the frame requested.
	 * 
	 * @param frame The frame to retrieve.
	 * @return A copy of the requested frame.
	 */
	public double[] getFrame(int frame)
	{
		double[] motion = new double[motions[frame].length];
		
		System.arraycopy(motions[frame], 0, motion, 0, motions[frame].length);
		
		return motion;
	}
	
	/**
	 * Returns the number of frames in this motion.
	 * 
	 * @return The number of frames in this motion.
	 */
	public int getFrames()
	{
		return motions.length;
	}
	
	/**
	 * Returns the value stored at a given frame and channel.
	 * 
	 * @param frame The frame where the requested value resides.
	 * @param channel The channel where the requested value resides.
	 * @return The value at the given frame and channel.
	 */
	public double getChannel(int frame, int channel)
	{
		return motions[frame][channel];
	}
	
	/**
	 * Returns a set of channels from a given frame.
	 * 
	 * @param frame The frame to retrieve values from.
	 * @param start The starting channel to retrieve values from.
	 * @param length The ending channel to retrieve values from.
	 * @return An array with all values in the requested frame from the start to end channel.
	 */
	public double[] getMotion(int frame, int start, int length)
	{
		double[] motion = new double[length];
		
		System.arraycopy(motions[frame], start, motion, 0, length);
		
		return motion;
	}
	
	/**
	 * Returns the length of a single frame in seconds.
	 * 
	 * @param length The length of a single frame in seconds.
	 */
	public void setFrameLength(double length)
	{
		frametime = length;
	}
	
	/**
	 * Sets the given frame/channel to the provided value.
	 * 
	 * @param frame The frame where the value is to be stored.
	 * @param channel The channel where the value is to be stored.
	 * @param value The value to store.
	 */
	public void setMotion(int frame, int channel, double value)
	{
		motions[frame][channel] = value;
	}
	
	/**
	 * Returns a string representation of this motion.  The string representation of this motion
	 * is directly equivalent to the motion section of a BVH file.
	 * 
	 * @return The string representation of this motion.
	 */
	public String toString()
	{
		String str = "";
		
		str += String.format("MOTION\n");
		str += String.format("Frames:     %d\n", motions.length);
		str += String.format("Frame Time: %12.10f\n", frametime);
		
		for(int frame = 0; frame < motions.length; frame++)
		{
			for(int channel = 0; channel < motions[frame].length; channel++)
			{
				str += String.format("%8.3f ", motions[frame][channel]);
			}
			str += String.format("\n");
		}
		
		return str;
	}

	public double getFrameTime()
	{
		return frametime;
	}
}