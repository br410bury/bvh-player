package net.br410bury.spacetime;

public enum Handedness
{
	LEFT  (-1),
	RIGHT (1);
	
	
	
	protected final int mult;
	
	
	
	/**
	 * Sets the multiplier for this handedness.
	 * 
	 * @param multiplier The multiplier for this handedness.
	 */
	Handedness(int multiplier)
	{
		this.mult = multiplier;
	}
	
	
	
	/**
	 * Returns the multiplier for this handedness.
	 * 
	 * @return The multiplier for this handedness.
	 */
	public int multiplier()
	{
		return mult;
	}
}