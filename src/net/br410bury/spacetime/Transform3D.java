package net.br410bury.spacetime;

public enum Transform3D
{
	XTran,
	YTran,
	ZTran,
	XRot,
	YRot,
	ZRot,
	XScale,
	YScale,
	ZScale,
	XYShear,
	XZShear,
	YXShear,
	YZShear,
	ZXShear,
	ZYShear;
	
	
	
	private double[][] xRot(double qty, double[][] ret)
	{
			ret[1][1] = ret[2][2] = Math.cos(qty);
			ret[1][2] = ret[2][1] = Math.sin(qty);
			ret[1][2] = -ret[1][2];
			
			return ret;
	}
	
	private double[][] yRot(double qty, double[][] ret)
	{
			ret[0][0] = ret[2][2] = Math.cos(qty);
			ret[0][2] = ret[2][0] = Math.sin(qty);
			ret[2][0] = -ret[2][0];
			
			return ret;
	}
	
	private double[][] zRot(double qty, double[][] ret)
	{
			ret[0][0] = ret[1][1] = Math.cos(qty);
			ret[0][1] = ret[1][0] = Math.sin(qty);
			ret[0][1] = -ret[0][1];
			
			return ret;
	}
	
	
	
	/**
	 * Returns a dour by four right-handed matrix for this particular transformation type
	 * given an amount to perform of this particular transformation.
	 * 
	 * @param qty The amount for this particular transformation type.
	 * @return A right-handed matrix 3D matrix of this type.
	 */
	public double[][] eval(double qty)
	{
		double[][] ret = new double[4][4];
		
		for(int i = 0; i < 4; i++) ret[i][i] = 1;
		switch(this)
		{
			case XTran:   ret[0][3] = qty; break;
			case YTran:   ret[1][3] = qty; break;
			case ZTran:   ret[2][3] = qty; break;
			case XRot:    xRot(qty, ret);  break;
			case YRot:    yRot(qty, ret);  break;
			case ZRot:    zRot(qty, ret);  break;
			case XScale:  ret[0][0] = qty; break;
			case YScale:  ret[1][1] = qty; break;
			case ZScale:  ret[2][2] = qty; break;
			case XYShear: ret[0][1] = qty; break;
			case XZShear: ret[0][2] = qty; break;
			case YXShear: ret[1][0] = qty; break;
			case YZShear: ret[1][2] = qty; break;
			case ZXShear: ret[2][0] = qty; break;
			case ZYShear: ret[2][1] = qty; break;
		}
		
		return ret;
	}
	
	/**
	 * Returns true if this is a rotation, false otherwise.
	 * 
	 * @return True if this is a rotation, false otherwise.
	 */
	public boolean isRotation()
	{
		return this == XRot || this == YRot || this == ZRot;
	}
	
	/**
	 * Returns true if this is a scale, false otherwise.
	 * 
	 * @return True if this is a scale, false otherwise.
	 */
	public boolean isScale()
	{
		return this == XScale || this == YScale || this == ZScale;
	}
	
	/**
	 * Returns true if this is a shear, false otherwise.
	 * 
	 * @return True if this is a shear, false otherwise.
	 */
	public boolean isShear()
	{
		return isXShear() || isYShear() || isZShear();
	}
	
	/**
	 * Returns true if this is a translation, false otherwise.
	 * 
	 * @return True if this is a translation, false otherwise.
	 */
	public boolean isTranslation()
	{
		return this == XTran || this == YTran || this == ZTran;
	}
	
	/**
	 * Returns true if this is a shear along x, false otherwise.
	 * 
	 * @return True if this is a shear along x, false otherwise.
	 */
	public boolean isXShear()
	{
		return this == XYShear || this == XZShear;
	}
	
	/**
	 * Returns true if this is a shear along y, false otherwise.
	 * 
	 * @return True if this is a shear along y, false otherwise.
	 */
	public boolean isYShear()
	{
		return this == YXShear || this == YZShear;
	}
	
	/**
	 * Returns true if this is a shear along z, false otherwise.
	 * 
	 * @return True if this is a shear along z, false otherwise.
	 */
	public boolean isZShear()
	{
		return this == ZXShear || this == ZYShear;
	}
	
	/**
	 * Returns a string representation of this object instance.
	 * 
	 * @return A string describing this transformation.
	 */
	public String toString()
	{
		String str = "";
						
		switch(this)
		{
			case XTran:   str = "X Translation";   break;
			case YTran:   str = "Y Translation";   break;
			case ZTran:   str = "Z Translation";   break;
			case XRot:    str = "X Rotation";      break;
			case YRot:    str = "Y Rotation";      break;
			case ZRot:    str = "Z Rotation";      break;
			case XScale:  str = "X Scale";         break;
			case YScale:  str = "Y Scale";         break;
			case ZScale:  str = "Z Scale";         break;
			case XYShear: str = "Shear Y along X"; break;
			case XZShear: str = "Shear Z along X"; break;
			case YXShear: str = "Shear X along Y"; break;
			case YZShear: str = "Shear Z along Y"; break;
			case ZXShear: str = "Shear X along Z"; break;
			case ZYShear: str = "Shear Y along Z"; break;
		}
		
		return str;
	}
}