package net.br410bury.lwjglex;

import org.lwjgl.opengl.Display;

public class LwjglCanvasThread extends Thread
{
	private LwjglCanvas canvas;
	private int syncspeed;
	
	public LwjglCanvasThread(LwjglCanvas canvas)
	{
		this.canvas = canvas;
		this.syncspeed = 60;
		setDaemon(true);
	}
	
	public void run()
	{
		while(true)
		{
			if(canvas.isVisible()) canvas.repaint();
			Display.sync(syncspeed);
		}
	}
	
	public void setCanvas(LwjglCanvas canvas)
	{
		this.canvas = canvas;
	}
	
	public void setSyncSpeed(int syncspeed)
	{
		this.syncspeed = syncspeed;
	}
}
