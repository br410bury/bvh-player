package net.br410bury.lwjglex;

import net.br410bury.graphics.GraphicsPoint;
import java.awt.Color;
import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.AWTGLCanvas;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

public abstract class LwjglCanvas extends AWTGLCanvas
{
	protected Component parent;
	protected LwjglCanvasThread redraw;
	protected Color color;
	protected float zoom;
	protected float fov;
	
	
	
	/**
	 * An empty constructor for use with extending this class.
	 * 
	 * @throws LWJGLException 
	 */
	public LwjglCanvas() throws LWJGLException
	{
		this.parent = null;
		this.color = Color.WHITE;
		this.redraw = new LwjglCanvasThread(this);
		this.zoom = 1;
		this.fov = 70;
	}
	
	/**
	 * Creates a new LWJGL Canvas with the specified parent component.
	 * 
	 * @param parent The parent component of this canvas.
	 * @throws LWJGLException 
	 */
	public LwjglCanvas(Component parent) throws LWJGLException
	{
		this.parent = parent;
		this.color = Color.WHITE;
		redraw = new LwjglCanvasThread(this);
		this.zoom = 1;
		this.fov = 70;
	}
	
	
	
	protected abstract void paintCanvas();
	
	
	
	protected void paintGL()
	{
		paintCanvas();
		swapBuffers();
	}
	
	
	
	/**
	 * Performs a default clear of the color buffer bit for OpenGL.
	 */
	public void clear()
	{
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
	}
	
	/**
	 * Draws a default axis with the X axis in red, Y axis in blue, and the Z axis in green.
	 */
	public void drawAxis()
	{
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		GL11.glBegin(GL11.GL_LINES);
			GL11.glColor3f(0.5f,0.0f,0.0f);
			GL11.glVertex3f(0.0f,0.0f,0.0f);
			GL11.glVertex3f(Float.MAX_VALUE,0.0f,0.0f);
			GL11.glColor3f(0.0f,0.5f,0.0f);
			GL11.glVertex3f(0.0f,0.0f,0.0f);
			GL11.glVertex3f(0.0f,Float.MAX_VALUE,0.0f);
			GL11.glColor3f(0.0f,0.0f,0.5f);
			GL11.glVertex3f(0.0f,0.0f,0.0f);
			GL11.glVertex3f(0.0f,0.0f,Float.MAX_VALUE);
		GL11.glEnd();
		GL11.glPopMatrix();
	}
	
	/**
	 * Draws a line from the point start to the point end.
	 * 
	 * @param start The starting point of the line to draw.
	 * @param end The ending point of the line to draw.
	 */
	public void drawLine(GraphicsPoint start, GraphicsPoint end)
	{
		float r = color.getRed()   / 255.0f;
		float g = color.getGreen() / 255.0f;
		float b = color.getBlue()  / 255.0f;
		
		GL11.glBegin(GL11.GL_LINES);
			GL11.glColor3f(r, g, b);
			GL11.glVertex3d(start.getX(), start.getY(), start.getZ());
			GL11.glVertex3d(end.getX(), end.getY(), end.getZ());
		GL11.glEnd();
	}
	
	/**
	 * Calculates and returns the current aspect ratio of the parent component.
	 * 
	 * @return The current aspect ratio of the parent component.
	 */
	public float getAspectRatio()
	{
		return (float)parent.getWidth() / (float)parent.getHeight();
	}
	
	/**
	 * Returns the current zoom factor.
	 * 
	 * @return The current zoom factor.
	 */
	public float getZoom()
	{
		return zoom;
	}
	
	/**
	 * Calls gluLookAt using the x, y, and z values stored in the points passed in.
	 * 
	 * @param eye The eye location for gluLookAt.
	 * @param center The center of projection for gluLookAt.
	 * @param up The up direction for gluLookAt.
	 */
	public void lookAt(GraphicsPoint eye, GraphicsPoint center, GraphicsPoint up)
	{
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		GLU.gluLookAt((float)eye.getX(),    (float)eye.getY(),    (float)eye.getZ(), 
						      (float)center.getX(), (float)center.getY(), (float)center.getZ(),
									(float)up.getX(),     (float)up.getY(),     (float)up.getZ());
	}
	
	/**
	 * Sets the color to draw all future points in.
	 * 
	 * @param r The red component.
	 * @param g The green component.
	 * @param b The blue component.
	 */
	public void setColor(double r, double g, double b)
	{
		color = new Color((float)r, (float)g, (float)b);
	}
	
	/**
	 * Sets the color to draw all future points in.
	 * 
	 * @param r The red component.
	 * @param g The green component.
	 * @param b The blue component.
	 */
	public void setColor(int r, int g, int b)
	{
		color = new Color(r, g, b);
	}
	
	/**
	 * Sets the parent component to the specified component.
	 * @param parent The parent component.
	 */
	public void setParent(Component parent)
	{
		this.parent = parent;
	}
	
	/**
	 * Sets the projection to orthogonal projection with the given min and max points.
	 * 
	 * @param min The minimum point of the ortho cube (top left near).
	 * @param max The maximum point of the ortho cube (bottom right far).
	 */
	public void setOrthogonal(GraphicsPoint min, GraphicsPoint max)
	{
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho((float)min.getX(), (float)max.getX(),
						     (float)min.getY(), (float)max.getY(),
								 (float)min.getZ(), (float)max.getZ());
	}
	
	/**
	 * Sets the projection matrix to perspective while taking into account the currently set
	 * zoom and field of view with the aspect ratio of the parent object, and the zNear and
	 * zFar supplied by the caller.
	 * 
	 * @param zNear The near plane of view.
	 * @param zFar The far plane of view.
	 */
	public void setPerspective(float zNear, float zFar)
	{
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(zoom * fov, getAspectRatio(), zNear, zFar);
	}
	
	/**
	 * Sets the viewport to the same size as the parent object.
	 */
	public void setDefaultViewport()
	{
		GL11.glViewport(0, 0, parent.getWidth(), parent.getHeight());
	}
	
	/**
	 * Sets the field of view for this canvas.
	 * 
	 * @param fov The field of view for projection.
	 */
	public void setFov(float fov)
	{
		this.fov = fov;
	}
	
	/**
	 * Sets the zoom parameter for use in the projection setup.
	 * 
	 * @param zoom The zoom factor for the projection setup.
	 */
	public void setZoom(float zoom)
	{
		this.zoom = zoom;
	}
	
	/**
	 * Begins the animation for this canvas.
	 */
	public void start()
	{
		redraw.start();
	}
	
	/**
	 * Swaps the buffers safely.
	 */
	public void swapBuffers()
	{
		try
		{
			super.swapBuffers();
		}
		catch(LWJGLException ex)
		{
			Logger.getLogger(LwjglCanvas.class.getName()).log(Level.WARNING, null, ex);
		}
	}
}
