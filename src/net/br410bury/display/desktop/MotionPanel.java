package net.br410bury.display.desktop;

import net.br410bury.formats.MotionFormat;
import net.br410bury.lwjglex.LwjglCanvas;
import java.awt.Component;
import org.lwjgl.LWJGLException;

public abstract class MotionPanel extends LwjglCanvas
{
	protected MotionFormat file;
	protected double azimuth = 1.50;
	protected double elevation = 0.6;
	protected int frame = 0;
	
	
	
	/**
	 * Sets up motion panel with the specified motion file and parent component.
	 * 
	 * @param parent The parent component of this panel.
	 * @param file The motion file for this component to draw.
	 * @throws LWJGLException 
	 */
	public MotionPanel(Component parent, MotionFormat file) throws LWJGLException
	{
		super(parent);
		this.file = file;
	}
	
	
	
	protected abstract void drawMotion();
	
	
	
	protected void paintCanvas()
	{
		drawMotion();
	}
	
	
	
	/**
	 * Updates the sync speed for this canvas.  Should only be called once the file has been
	 * read.
	 */
	public void setSyncSpeed()
	{
		int syncspeed = 0;
		
		if(file.isRead())
		{
			syncspeed = (int)(1.0 / file.getMotion().getFrameTime());
			redraw.setSyncSpeed(syncspeed);
		}
	}
}