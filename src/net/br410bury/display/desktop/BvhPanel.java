package net.br410bury.display.desktop;

import net.br410bury.formats.BvhFormat;
import net.br410bury.graphics.GraphicsPoint;
import net.br410bury.motion.Bone;
import java.awt.Component;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.LWJGLException;

public class BvhPanel extends MotionPanel
{
	/**
	 * Sets up a new BVH Panel with the given parent and motion file.
	 * 
	 * @param parent The parent component of this panel.
	 * @param file A BVH file.
	 * @throws LWJGLException
	 */
	public BvhPanel(Component parent, BvhFormat file) throws LWJGLException
	{
		super(parent, file);
	}
	
	/**
	 * Sets up a new BVH Panel with the given parent and motion file.
	 * 
	 * @param parent The parent component of this panel.
	 * @param file The location of a BVH file.
	 * @throws LWJGLException
	 */
	public BvhPanel(Component parent, String filename) throws LWJGLException, IOException
	{
		super(parent, new BvhFormat());
		
		file.read(filename);
	}
	
	
	
	protected GraphicsPoint calcEye(GraphicsPoint min, GraphicsPoint max, GraphicsPoint center)
	{
		GraphicsPoint eye = new GraphicsPoint();
		double x = Math.pow(max.getX() - min.getX(), 2);
		double y = Math.pow(max.getY() - min.getY(), 2);
		double azi = azimuth * Math.sqrt(x + y);
			
		eye.setX(azi + center.getX());
		eye.setY(elevation * center.getY());
		eye.setZ(azi + center.getZ());
		
		return eye;
	}
	
	protected void drawMotion()
	{
		Bone boneframe;
		GraphicsPoint min;
		GraphicsPoint max;
		GraphicsPoint eye;
		GraphicsPoint center;
		GraphicsPoint up;
		
		if(file.isRead())
		{
			file.segment(frame);
			
			boneframe = file.getSkeleton(frame);
			min = boneframe.getMins();
			max = boneframe.getMaxes();
			
			center = GraphicsPoint.center(min, max);
			eye = calcEye(min, max, center);
			up = new GraphicsPoint(0, 1, 0);
			
			setSyncSpeed();
			clear();
			
			setPerspective(0, 100);
			lookAt(eye, center, up);
			setDefaultViewport();
			
			drawAxis();
			drawBone(file.getSkeleton(frame));
			frame = (frame + 1) % file.getMotion().getFrames();
		}
		else
		{
			Exception ex = new Exception("File has not been properly read.");
			Logger.getLogger(BvhPanel.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	protected void drawBone(Bone bone)
	{
		GraphicsPoint start;
		GraphicsPoint end;
			
		start = bone.getStart();
		for(Bone child : bone.getEnds())
		{
			end = child.getStart();
			drawLine(start, end);
			drawBone(child);
		}
	}
}